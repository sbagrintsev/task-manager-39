package ru.tsc.bagrintsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Insert("INSERT INTO m_task " +
            "(id, date_created, name, description, status, user_id, project_id, date_started, date_finished) " +
            "VALUES " +
            "(#{id}, #{dateCreated}, #{name}, #{description}, #{status}, #{userId}, #{projectId}, #{dateStarted}, #{dateFinished};")
    void add(@NotNull final Task record);

    @Insert({
            "<script>",
            "INSERT INTO m_task",
            "(id, date_created, name, description, status, user_id, project_id, date_started, date_finished) ",
            "VALUES" +
                    "<foreach item='item' collection='records' open='' separator=',' close=''>" +
                    "(" +
                    "#{item.id},",
            "#{item.dateCreated},",
            "#{item.name},",
            "#{item.description},",
            "#{item.status},",
            "#{item.userId},",
            "#{item.projectId},",
            "#{item.dateStarted},",
            "#{item.dateFinished}" +
                    ")" +
                    "</foreach>",
            "</script>"})
    void addAll(@Param("records") @NotNull final Collection<Task> records);

    @Override
    @Delete("DELETE " +
            "FROM m_task " +
            "WHERE user_id = #{userId};")
    void clear(@Param("userId") @NotNull final String userId);

    @Override
    @Delete("DELETE " +
            "FROM m_task;")
    void clearAll();

    @Override
    @Select("SELECT count(*) = 1 " +
            "FROM m_task " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    boolean existsById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, project_id, date_started, date_finished " +
            "FROM m_task;")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, project_id, date_started, date_finished " +
            "FROM m_task " +
            "WHERE user_id = #{userId} " +
            "AND project_id = #{projectId};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<Task> findAllByProjectId(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId
    );

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, project_id, date_started, date_finished " +
            "FROM m_task " +
            "WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<Task> findAllByUserId(@Param("userId") @NotNull final String userId);

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, project_id, date_started, date_finished " +
            "FROM m_task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY #{order};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    List<Task> findAllSort(
            @Param("userId") @NotNull final String userId,
            @Param("order") @NotNull final String order
    );

    @Override
    @Nullable
    @Select("SELECT id, date_created, name, description, status, user_id, project_id, date_started, date_finished " +
            "FROM m_task " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    @Results(value = {
            @Result(property = "dateCreated", column = "date_created"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStarted", column = "date_started"),
            @Result(property = "dateFinished", column = "date_finished")
    })
    Task findOneById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Override
    @Delete("DELETE" +
            "FROM m_task " +
            "WHERE user_id = #{userId} " +
            "AND id = #{id};")
    void removeById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id
    );

    @Update("UPDATE m_task " +
            "SET project_id = #{projectId} " +
            "WHERE user_id = #{userId} " +
            "AND id = #{taskId};")
    void setProjectId(
            @Param("userId") @NotNull final String userId,
            @Param("taskId") @NotNull final String taskId,
            @Param("projectId") @Nullable final String projectId
    );

    @Override
    @Select("SELECT count(*) " +
            "FROM m_task;")
    long totalCount();

    @Override
    @Select("SELECT count(*) " +
            "FROM m_task " +
            "WHERE user_id = #{userId};")
    long totalCountByUserId(@Param("userId") @NotNull final String userId);

    @Override
    @Update("UPDATE m_task " +
            "SET name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}, " +
            "project_id = #{projectId}, " +
            "user_id = #{userid}, " +
            "date_started = #{dateStarted}, " +
            "date_finished = #{dateFinished} " +
            "WHERE id = #{id};")
    void update(@NotNull final Task task);

    @Update("UPDATE m_task " +
            "SET name = #{name}, " +
            "description = #{description} " +
            "WHERE user_id = #{userid}" +
            "AND id = #{id};")
    void updateById(
            @Param("userId") @Nullable final String userId,
            @Param("id") @Nullable final String id,
            @Param("name") @Nullable final String name,
            @Param("description") @Nullable final String description
    );

}
