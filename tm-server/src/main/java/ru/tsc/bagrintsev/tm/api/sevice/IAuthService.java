package ru.tsc.bagrintsev.tm.api.sevice;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.Session;

import java.security.GeneralSecurityException;

public interface IAuthService {

    @NotNull
    String signIn(
            @Nullable String login,
            @Nullable String password
    ) throws JsonProcessingException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, PasswordIsIncorrectException, UserNotFoundException;

    @NotNull
    Session validateToken(@Nullable String token) throws AccessDeniedException, JsonProcessingException;

}
