package ru.tsc.bagrintsev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IAbstractService;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public SqlSession getSession() {
        return connectionService.getSqlSession();
    }

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    public abstract long totalCount();

}
