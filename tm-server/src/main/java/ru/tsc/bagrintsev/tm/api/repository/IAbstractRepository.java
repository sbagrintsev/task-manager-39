package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    void addAll(@NotNull final Collection<M> records);

    void clearAll();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    long totalCount();

}
