package ru.tsc.bagrintsev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.component.Bootstrap;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;

import java.io.IOException;

/**
 * @author Sergey Bagrintsev
 * @version 1.39.0
 */

public final class Application {

    public static void main(@Nullable final String[] args) throws DomainNotFoundException, IOException, ClassNotFoundException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
