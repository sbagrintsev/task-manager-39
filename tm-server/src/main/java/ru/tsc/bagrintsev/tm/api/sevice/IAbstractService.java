package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IAbstractService<M extends AbstractModel> {

    void clearAll();

    @NotNull
    List<M> findAll();

    @NotNull
    Collection<M> set(@NotNull final Collection<M> records);

}
