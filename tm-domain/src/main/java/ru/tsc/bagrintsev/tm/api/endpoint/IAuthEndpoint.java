package ru.tsc.bagrintsev.tm.api.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.security.GeneralSecurityException;
import java.sql.SQLException;

@WebService
public interface IAuthEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserSignInResponse signIn(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSignInRequest request
    ) throws AbstractException, GeneralSecurityException, JsonProcessingException;

    @NotNull
    @WebMethod
    UserSignOutResponse signOut(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSignOutRequest request
    );

    @NotNull
    @WebMethod
    UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    );

}
